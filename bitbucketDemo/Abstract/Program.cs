﻿using Abstract.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abstract
{
    class Program
    {
        static void Main(string[] args)
        {
            Process pro = new Process();
            pro.DoWork(new ProcessRequest { });
        }
    }
}
