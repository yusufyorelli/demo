﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abstract.Service
{
    public class Process : ServiceBase<ProcessRequest, ProcessResponse>
    {
        public override ProcessResponse DoAssign(int s1)
        {
            ProcessResponse response = new ProcessResponse();
            response.Message = "OK";
            return response;
        }

        public override int DoRequest(ProcessRequest input)
        {
            return 30;
        }

        //public ProcessResponse DoWork(ProcessRequest input)
        //{
        //    ProcessResponse response = new ProcessResponse();
        //    response.Message = "OKTO";

        //    return response;
        //}
    }
}
