﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abstract.Service
{
    public abstract class ServiceBase<TInput, TReturn>
        where TInput : class
        where TReturn : class
    {
        public abstract int DoRequest(TInput input)
        {
            DoAssign(input);

            return 1;
        }
        public abstract TReturn DoAssign(TInput input);


        public TReturn DoWork(TInput input)
        {
            TReturn result = null;

            int s1 = 0;
            s1 = DoRequest(input);

            result = DoAssign(input);

            return result;
        }
    }
}
