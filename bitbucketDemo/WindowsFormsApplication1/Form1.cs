﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{

    public class Publisher
    {
        private readonly RabbitMQService _rabbitMQService;

        public Publisher(string queueName, string message)
        {
            _rabbitMQService = new RabbitMQService();

            using (var connection = _rabbitMQService.GetRabbitMQConnection())
            {
                using (var channel = connection.CreateModel())
                {
                    channel.QueueDeclare(queueName, false, false, false, null);

                    channel.BasicPublish("", queueName, null, Encoding.UTF8.GetBytes(message));

                }
            }
        }
    }

    public class RabbitMQService
    {
        // localhost üzerinde kurulu olduğu için host adresi olarak bunu kullanıyorum.

        public IConnection GetRabbitMQConnection()
        {
            ConnectionFactory connectionFactory = new ConnectionFactory()
            {
                DispatchConsumersAsync = true,
                HostName = "localhost"
            };

            return connectionFactory.CreateConnection();
        }
    }

    public static async  Task Consumer_Received(object sender, BasicDeliverEventArgs @event)
    {
        var message = Encoding.UTF8.GetString(@event.Body);

        Console.WriteLine($"Begin processing {message}");

        await Task.Delay(250);

        Console.WriteLine($"End processing {message}");
    }

    public class Consumer
    {
        private readonly RabbitMQService _rabbitMQService;


        public Consumer(string queueName, Label lbl)
        {
            _rabbitMQService = new RabbitMQService();

            using (var connection = _rabbitMQService.GetRabbitMQConnection())
            {
                using (var channel = connection.CreateModel())
                {
                    var consumer = new AsyncEventingBasicConsumer(channel);
                    // Received event'i sürekli listen modunda olacaktır.

                    consumer.Received += Consumer_Received;


                    //consumer.Received += async (model, ea) =>
                    //{
                    //    var body = ea.Body;
                    //    var message = Encoding.UTF8.GetString(body);

                    //    lbl.Text = message;

                    //    Thread.Sleep(500);
                    //    await Task.Yield();

                    //};

                    channel.BasicConsume(queueName, false, consumer);

                }
            }
        }

    }


    public partial class Form1 : Form
    {
        private static readonly string _queueName = "GOKHANGOKALP";
        private static Publisher _publisher;
        private static Consumer _consumer;

        public Form1()
        {
            InitializeComponent();



            _consumer = new Consumer(_queueName, label1);

            int s1 = 1;
            int s2 = 2;
            int s3 = 3;

        }
    }
}
