﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static multiThread.Program;

namespace multiThread
{
    class Program
    {
        //private static SemaphoreSlim semaphore;
        //// A padding interval to make the output more orderly.
        //private static int audience;

        //public static async void run()
        //{
        //    semaphore = new SemaphoreSlim(1,3);
        //    Console.WriteLine("{0} tasks can enter the semaphore.",
        //                      semaphore.CurrentCount);

        //    const int SIZE = 1;
        //    Task[] tasks = new Task[SIZE]; 

        //    // Create and start five numbered tasks.
        //    for (int i = 0; i <= SIZE - 1; i++)
        //    {
        //        tasks[i] = Task.Run(() => {
        //            // Each task begins by requesting the semaphore.
        //            Console.WriteLine("Task {0} begins and waits for the semaphore, audience count: {1}",
        //                            Task.CurrentId, audience);
        //            semaphore.Wait();

        //            Interlocked.Increment(ref audience);

        //            Console.WriteLine("Task {0} enters the semaphore , audience count: {1}", Task.CurrentId, audience);

        //            Thread.Sleep(1000);

        //            Interlocked.Decrement(ref audience);
        //            Console.WriteLine("Task {0} releases the semaphore; previous count: {1} , audience count: {2}",
        //                            Task.CurrentId, semaphore.Release(), audience);

        //        });



        //    }

        //    // Wait for half a second, to allow all the tasks to start and block.
        //    Thread.Sleep(500);

        //    // Restore the semaphore count to its maximum value.
        //    Console.Write("Main thread calls Release(3) --> ");
        //    semaphore.Release();
        //    Console.WriteLine("{0} tasks can enter the semaphore.",
        //                      semaphore.CurrentCount);
        //    // Main thread waits for the tasks to complete.
        //    Task.WaitAll(tasks);

        //    Console.WriteLine("Main thread exits.");
        //}

        public static void Main()
        {
            List<Sinif> snf = new List<Sinif>();

            snf.Add(new Sinif { Ad = "Cemil 1", SleepTime = 8000 });
            snf.Add(new Sinif { Ad = "Kamil 2", SleepTime = 8000 });
            snf.Add(new Sinif { Ad = "İsmail 3", SleepTime = 8000 });
            snf.Add(new Sinif { Ad = "Demir 4", SleepTime = 8000 });
            snf.Add(new Sinif { Ad = "Onur 5", SleepTime = 5000 });

            snf.Add(new Sinif { Ad = "Canan 6", SleepTime = 5000 });
            snf.Add(new Sinif { Ad = "Tuncay 7", SleepTime = 5000 });
            snf.Add(new Sinif { Ad = "Kezban 8", SleepTime = 5000 });
            snf.Add(new Sinif { Ad = "Ozan 9", SleepTime = 5000 });


            List<Jamiyo> jmr = new List<Jamiyo>();

            jmr.Add(new Jamiyo { name = "J Cemil 1", SleepTime = 7000 });
            jmr.Add(new Jamiyo { name = "J Kamil 2", SleepTime = 7000 });
            jmr.Add(new Jamiyo { name = "J İsmail 3", SleepTime = 7000 });
            jmr.Add(new Jamiyo { name = "J Demir 4", SleepTime = 3000 });
            jmr.Add(new Jamiyo { name = "J Onur 5", SleepTime = 4000 });
            jmr.Add(new Jamiyo { name = "J Cemil 1", SleepTime = 7000 });
            jmr.Add(new Jamiyo { name = "J Kamil 2", SleepTime = 7000 });
            jmr.Add(new Jamiyo { name = "J İsmail 3", SleepTime = 1000 });
            jmr.Add(new Jamiyo { name = "J Demir 4", SleepTime = 4000 });
            jmr.Add(new Jamiyo { name = "J Onur 5", SleepTime = 2000 });
            jmr.Add(new Jamiyo { name = "J Cemil 1", SleepTime = 8000 });
            jmr.Add(new Jamiyo { name = "J Kamil 2", SleepTime = 3000 });
            jmr.Add(new Jamiyo { name = "J İsmail 3", SleepTime = 7000 });
            jmr.Add(new Jamiyo { name = "J Demir 4", SleepTime = 6000 });
            jmr.Add(new Jamiyo { name = "J Onur 5", SleepTime = 7000 });

            jmr.Add(new Jamiyo { name = "J Canan 6", SleepTime = 7000 });
            jmr.Add(new Jamiyo { name = "J Tuncay 7", SleepTime = 7000 });
            jmr.Add(new Jamiyo { name = "J Kezban 8", SleepTime = 7000 });
            jmr.Add(new Jamiyo { name = "J Ozan 9", SleepTime = 7000 });


            //run();
            //Thread.Sleep(200);
            //Console.WriteLine("fdsfdsf");  

            //var result = ForEachAsyncJob<Sinif>(snf, new Func<Sinif, Task>(Sinif.yaz));
             var result2 = ForEachAsyncJob<Jamiyo>(jmr, new Func<Jamiyo, Task>(Jamiyo.yaz),4);

            //Console.WriteLine("" + DateTime.Now.ToLongTimeString());
            //Thread.Sleep(1000);
            //Console.WriteLine("" + DateTime.Now.ToLongTimeString());
            //Thread.Sleep(5000);
            //Console.WriteLine("" + DateTime.Now.ToLongTimeString());
            Thread.Sleep(50000);
            Console.ReadLine();
        }

        public static Task ForEachAsyncJob<TIn>(
            List<TIn> Tinput,
            Func<TIn, Task> asyncMethodProcessor, int maxThread = 2)
        {

            SemaphoreSlim _pool = new SemaphoreSlim(maxThread);

            List<Task> tasks = Tinput.Select(async input =>
            {
                await _pool.WaitAsync().ConfigureAwait(false);
                try
                {
                    await asyncMethodProcessor(input).ConfigureAwait(false);
                }
                finally
                {
                    _pool.Release();
                }
            }).ToList();

            return Task.WhenAll(tasks);
        }

        public class Jamiyo
        {
            public string name { get; set; }
            public string lastName { get; set; }
            public int SleepTime { get; set; }

            public static Task yaz(Jamiyo d)
            {

                int t = 0;
                var returnTask = Task.Run(() =>
                {
                    int Sayac = 0;

                    string correct = d.name;

                    int t1 = 0;

                    for (int i = 0; i < 5; i++)
                    {
                        Thread.Sleep(60);
                        t1++;
                    }


                    Console.WriteLine("Jamiryo: " + d.name + " Başladı  COUNT" + t1.ToString());
                    Thread.Sleep(d.SleepTime);

                    //Console.WriteLine();
                    Console.WriteLine("Jamiryo: " + d.name + " Bitti");
                });

                return returnTask;
            }

        }
        public class Sinif
        {
            public string Ad { get; set; }
            public int SleepTime { get; set; }

            public static Task yaz(Sinif d)
            {

                int t = 0;
                var returnTask = Task.Run(() =>
               {
                   int Sayac = 0;


                   Console.WriteLine("Sinif: " + d.Ad + " Başladı");
                   Thread.Sleep(d.SleepTime);
                   //Console.WriteLine();
                   Console.WriteLine("Sinir: " + d.Ad + " Bitti");
               });

                return returnTask;
            }
        }
    }
}
