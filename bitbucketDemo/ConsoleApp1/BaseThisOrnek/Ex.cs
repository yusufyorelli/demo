﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1.BaseThisOrnek
{
    class A
    {
        public A(int a)
        {
            Console.WriteLine("A Temel sınıfının yapıcı metodu çalıştı. Parametre: {0}", a);
        }


    }

    class B : A
    {
        public B(int a, int b) : base(a)
        {
            Console.WriteLine("Türetilmiş B sınıfının yapıcı metodu çalıştı. Parametre: {0}", b);
        }


    }

    class C : B
    {
        public C(int a, int b, int c) : base(a, b)
        {
            Console.WriteLine("Türetilmiş C sınıfının yapıcı metodu çalıştı. Parametre: {0}", c);
        }
    }


    class Silindir
    {
        private double r;
        private double h;
        private double PI;

        public Silindir(double yaricap, double yukseklik, double piSayisi)
        {
            r = yaricap;
            h = yukseklik;
            PI = piSayisi;
        }

        public Silindir(double yaricap, double yukseklik) : this(yaricap, yukseklik, 3.1415) { }

        public double Hacim()
        {
            return (PI * r * r * h);
        }

        public double YuzeyAlani()
        {
            return (2 * PI * r * h);
        }
    }


    public class s1
    {
        public s1(int a, int b) : this(b)
        {
            Console.WriteLine(a.ToString() + "  -   " + b.ToString());
        }

        public s1(int a)
        {
            Console.WriteLine(a.ToString());
        }
    }
}
